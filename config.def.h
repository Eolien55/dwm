/* See LICENSE file for copyright and license details. */

// Xf86 keys
#include <X11/XF86keysym.h>

/* appearance */
static unsigned int borderpx  = 1;        /* border pixel of windows */
static unsigned int snap      = 32;       /* snap pixel */
static unsigned int gappih    = 20;       /* horiz inner gap between windows */
static unsigned int gappiv    = 10;       /* vert inner gap between windows */
static unsigned int gappoh    = 10;       /* horiz outer gap between windows and screen edge */
static unsigned int gappov    = 30;       /* vert outer gap between windows and screen edge */
static       int smartgaps          = 0;        /* 1 means no outer gap when there is only one window */
static int showbar            = 1;        /* 0 means no bar */
static int topbar             = 1;        /* 0 means bottom bar */
static char font[]            = "monospace:size=10";
static char dmenufont[]       = "monospace:size=10";
static char *fonts[]          = { font };
static char normbgcolor[]           = "#222222";
static char normbordercolor[]       = "#444444";
static char normfgcolor[]           = "#bbbbbb";
static char selfgcolor[]            = "#eeeeee";
static char selbordercolor[]        = "#005577";
static char selbgcolor[]            = "#005577";
static char termcol0[] = "#000000"; /* black   */
static char termcol1[] = "#ff0000"; /* red     */
static char termcol2[] = "#33ff00"; /* green   */
static char termcol3[] = "#ff0099"; /* yellow  */
static char termcol4[] = "#0066ff"; /* blue    */
static char termcol5[] = "#cc00ff"; /* magenta */
static char termcol6[] = "#00ffff"; /* cyan    */
static char termcol7[] = "#d0d0d0"; /* white   */
static char termcol8[]  = "#808080"; /* black   */
static char termcol9[]  = "#ff0000"; /* red     */
static char termcol10[] = "#33ff00"; /* green   */
static char termcol11[] = "#ff0099"; /* yellow  */
static char termcol12[] = "#0066ff"; /* blue    */
static char termcol13[] = "#cc00ff"; /* magenta */
static char termcol14[] = "#00ffff"; /* cyan    */
static char termcol15[] = "#ffffff"; /* white   */
static char termcol16[] = "#ffffff"; /* orange  */
static char termcol17[] = "#ffffff"; /* orange  */
static char *termcolor[] = {
  termcol0,
  termcol1,
  termcol2,
  termcol3,
  termcol4,
  termcol5,
  termcol6,
  termcol7,
  termcol8,
  termcol9,
  termcol10,
  termcol11,
  termcol12,
  termcol13,
  termcol14,
  termcol15,
  termcol16,
  termcol17,
  selbgcolor,
  selfgcolor
};
static char *colors[][3] = {
       /*               fg           bg           border   */
       [SchemeNorm] = { normfgcolor, normbgcolor, normbordercolor },
       [SchemeSel]  = { selfgcolor,  selbgcolor,  selbordercolor  },
};

/* tagging */
static const char *tags[] = { "1", "2", "3", "4", "5", "6", "7", "8", "9" };

static const Rule rules[] = {
	/* xprop(1):
	 *	WM_CLASS(STRING) = instance, class
	 *	WM_NAME(STRING) = title
	 */
	/* class      instance    title       tags mask     isfloating   monitor */
	{ "Gimp",     NULL,       NULL,       0,            1,           -1 },
	{ "Firefox",  NULL,       NULL,       1 << 8,       0,           -1 },
};

/* layout(s) */
static float mfact     = 0.55; /* factor of master area size [0.05..0.95] */
static int nmaster     = 1;    /* number of clients in master area */
static int resizehints = 1;    /* 1 means respect size hints in tiled resizals */
static int lockfullscreen = 1; /* 1 will force focus on the fullscreen window */

#define FORCE_VSPLIT 1  /* nrowgrid layout: force two clients to always split vertically */
#include "vanitygaps.c"

static const Layout layouts[] = {
	/* symbol     arrange function */
	{ "[\\]",     dwindle },	/* first entry is default */
	{ "[]=",      tile },
	{ "[M]",      monocle },
	{ "[@]",      spiral },
	{ "H[]",      deck },
	{ "TTT",      bstack },
	{ "===",      bstackhoriz },
	{ "HHH",      grid },
	{ "###",      nrowgrid },
	{ "---",      horizgrid },
	{ ":::",      gaplessgrid },
	{ "|M|",      centeredmaster },
	{ ">M>",      centeredfloatingmaster },
	{ "><>",      NULL },    /* no layout function means floating behavior */
	{ NULL,       NULL },
};

/* key definitions */
#define MODKEY Mod4Mask
#define TAGKEYS(KEY,TAG) \
	{ 1, {{MODKEY,                       KEY}},      view,           {.ui = 1 << TAG} }, \
	{ 1, {{MODKEY|ControlMask,           KEY}},      toggleview,     {.ui = 1 << TAG} }, \
	{ 1, {{MODKEY|ShiftMask,             KEY}},      tag,            {.ui = 1 << TAG} }, \
	{ 1, {{MODKEY|ControlMask|ShiftMask, KEY}},      toggletag,      {.ui = 1 << TAG} },

/* helper for spawning shell commands in the pre dwm-5.0 fashion */
#define term "st"
#define SHCMD(cmd) { .v = (const char*[]){ "/bin/sh", "-c", cmd, NULL } }
/* helper for dmenu-based scripts */
#define DMENUCMD(cmd) { .v = (const char*[]){ cmd, "-fn", dmenufont, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbordercolor, "-sf", selfgcolor, NULL } }
/* start prog in terminal */
#define TERMCMD(cmd) SHCMD(term " -e " cmd)

#define STATUSBAR "dwmblocks"

/* commands */
static const char *dmenucmd[] = { "dmenu_run", "-fn", dmenufont, "-nb", normbgcolor, "-nf", normfgcolor, "-sb", selbordercolor, "-sf", selfgcolor, NULL };
static const char *termcmd[]  = { term, NULL };

/*
 * Xresources preferences to load at startup
 */
ResourcePref resources[] = {
		{ "font",               STRING,  &font },
		{ "font",               STRING,  &dmenufont },
		{ "dmenufont",          STRING,  &dmenufont },
		{ "normbgcolor",        STRING,  &normbgcolor },
		{ "normbordercolor",    STRING,  &normbordercolor },
		{ "normfgcolor",        STRING,  &normfgcolor },
		{ "selbgcolor",         STRING,  &selbgcolor },
		{ "selbordercolor",     STRING,  &selbordercolor },
		{ "selfgcolor",         STRING,  &selfgcolor },
		{ "borderpx",          	INTEGER, &borderpx },
		{ "snap",          	INTEGER, &snap },
		{ "showbar",          	INTEGER, &showbar },
		{ "topbar",          	INTEGER, &topbar },
		{ "nmaster",          	INTEGER, &nmaster },
		{ "resizehints",       	INTEGER, &resizehints },
		{ "mfact",      	FLOAT,   &mfact },
		{ "color0",		STRING,  &termcol0 },
		{ "color1",		STRING,  &termcol1 },
		{ "color2",		STRING,  &termcol2 },
		{ "color3",		STRING,  &termcol3 },
		{ "color4",		STRING,  &termcol4 },
		{ "color5",		STRING,  &termcol5 },
		{ "color6",		STRING,  &termcol6 },
		{ "color7",		STRING,  &termcol7 },
		{ "color8",		STRING,  &termcol8 },
		{ "color9",		STRING,  &termcol9 },
		{ "color10",		STRING,  &termcol10 },
		{ "color11",		STRING,  &termcol11 },
		{ "color12",		STRING,  &termcol12 },
		{ "color13",		STRING,  &termcol13 },
		{ "color14",		STRING,  &termcol14 },
		{ "color15",		STRING,  &termcol15 },
		{ "color16",		STRING,  &termcol16 },
		{ "color17",		STRING,  &termcol17 },
		{ "gappih",		INTEGER, &gappih },
		{ "gappiv",		INTEGER, &gappiv },
		{ "gappoh",		INTEGER, &gappoh },
		{ "gappov",		INTEGER, &gappov },
};

#include "movestack.c"
static Keychord keychords[] = {
	/* modifier                     key        function        argument */
	{ 1, {{MODKEY,                  XK_g}},				spawn,          {.v = dmenucmd } },
	{ 1, {{MODKEY,                  XK_Return}},			spawn,          SHCMD(term) },
	{ 1, {{MODKEY,                  XK_p}},				spawn,          DMENUCMD("passmenu") },
	{ 1, {{MODKEY|ControlMask,      XK_c}},				spawn,          DMENUCMD("change-theme") },
	{ 1, {{MODKEY,                  XK_n}},				spawn,          TERMCMD("newsboat") },
	{ 1, {{MODKEY|ShiftMask,        XK_n}},				spawn,          TERMCMD("vimpc") },
	{ 1, {{MODKEY,                  XK_x}},				spawn,          DMENUCMD("_reviser") },
	{ 1, {{MODKEY|ShiftMask,	XK_i}},				spawn,          DMENUCMD("inputmenu") },
	{ 1, {{MODKEY|ShiftMask,	XK_d}},				spawn,	   	TERMCMD("diary new") },
	{ 1, {{MODKEY,			XK_w}},				spawn,          SHCMD("$DWM_BROWSER") },
	{ 1, {{MODKEY|ShiftMask,	XK_s}},				spawn,          SHCMD("syssus") },
	{ 1, {{0, 			XF86XK_AudioRaiseVolume}},	spawn,          SHCMD("pulsemixer --change-volume +1;	kill -44 $(pidof dwmblocks)") },
	{ 1, {{0, 			XF86XK_AudioLowerVolume}},	spawn,          SHCMD("pulsemixer --change-volume -1;	kill -44 $(pidof dwmblocks)") },
	{ 1, {{0, 			XF86XK_AudioMute}},		spawn,          SHCMD("pulsemixer --toggle-mute;	kill -44 $(pidof dwmblocks)") },
	{ 2, {{MODKEY, XK_v}, {0,	XK_plus}},			spawn,		SHCMD("pulsemixer --change-volume +5;	kill -44 $(pidof dwmblocks)") },
	{ 2, {{MODKEY, XK_v}, {0,	XK_minus}},			spawn,		SHCMD("pulsemixer --change-volume -5;	kill -44 $(pidof dwmblocks)") },
	{ 2, {{MODKEY, XK_v}, {0,	XK_n}},				spawn,		SHCMD("pulsemixer --change-volume +10;	kill -44 $(pidof dwmblocks)") },
	{ 2, {{MODKEY, XK_v}, {0,	XK_p}},				spawn,		SHCMD("pulsemixer --change-volume -10;	kill -44 $(pidof dwmblocks)") },
	{ 2, {{MODKEY, XK_v}, {0,	XK_m}},				spawn,		SHCMD("pulsemixer --toggle-mute;	kill -44 $(pidof dwmblocks)") },
	{ 1, {{0,			XF86XK_AudioPlay}},		spawn,          SHCMD("mpc toggle") },
	{ 1, {{0,			XF86XK_AudioNext}},		spawn,          SHCMD("mpc next") },
	{ 1, {{0,			XF86XK_AudioPrev}},		spawn,          SHCMD("mpc prev") },
	{ 2, {{MODKEY, XK_m}, {0,	XK_m}},				spawn,		SHCMD("mpc toggle") },
	{ 2, {{MODKEY, XK_m}, {0,	XK_n}},				spawn,		SHCMD("mpc next") },
	{ 2, {{MODKEY, XK_m}, {0,	XK_p}},				spawn,		SHCMD("mpc prev") },
	{ 2, {{MODKEY, XK_m}, {0,	XK_v}},				spawn,		TERMCMD("vimpc") },
	{ 1, {{0, 			XK_Print}},			spawn,          SHCMD("screenshot") },
	{ 1, {{0|ShiftMask, 		XK_Print}},			spawn,          SHCMD("screenshot -s") },
	{ 1, {{MODKEY,			XK_b}},				togglebar,      {0} },
	{ 1, {{MODKEY,			XK_j}},				focusstack,     {.i = +1 } },
	{ 1, {{MODKEY,			XK_k}},				focusstack,     {.i = -1 } },
	{ 1, {{MODKEY,			XK_i}},				incnmaster,     {.i = +1 } },
	{ 1, {{MODKEY,			XK_d}},				incnmaster,     {.i = -1 } },
	{ 1, {{MODKEY,			XK_h}},				setmfact,       {.f = -0.05} },
	{ 1, {{MODKEY,			XK_l}},				setmfact,       {.f = +0.05} },
	{ 1, {{MODKEY|ShiftMask,	XK_h}},				setcfact,       {.f = +0.25} },
	{ 1, {{MODKEY|ShiftMask,	XK_l}},				setcfact,       {.f = -0.25} },
	{ 1, {{MODKEY|ShiftMask,	XK_o}},				setcfact,       {.f =  0.00} },
	{ 1, {{MODKEY|ShiftMask,	XK_j}},				movestack,      {.i = +1 } },
	{ 1, {{MODKEY|ShiftMask,	XK_k}},				movestack,      {.i = -1 } },
	{ 1, {{MODKEY,			XK_Tab}},			view,           {0} },
	{ 1, {{MODKEY,			XK_q}},				killclient,     {0} },
	{ 2, {{MODKEY, XK_t}, {0,	XK_a}},				setlayout,      {.v = &layouts[0]} },
	{ 2, {{MODKEY, XK_t}, {0,	XK_z}},				setlayout,      {.v = &layouts[1]} },
	{ 2, {{MODKEY, XK_t}, {0,	XK_e}},				setlayout,      {.v = &layouts[2]} },
	{ 2, {{MODKEY, XK_t}, {0,	XK_r}},				setlayout,      {.v = &layouts[3]} },
	{ 2, {{MODKEY, XK_t}, {0,	XK_t}},				setlayout,      {.v = &layouts[4]} },
	{ 2, {{MODKEY, XK_t}, {0,	XK_y}},				setlayout,      {.v = &layouts[5]} },
	{ 2, {{MODKEY, XK_t}, {0,	XK_u}},				setlayout,      {.v = &layouts[6]} },
	{ 2, {{MODKEY, XK_t}, {0,	XK_n}},				cyclelayout,    {.i = +1 } },
	{ 2, {{MODKEY, XK_t}, {0,	XK_p}},				cyclelayout,    {.i = -1 } },
	{ 1, {{MODKEY,			XK_space}},			zoom,           {0} },
	{ 1, {{MODKEY|ShiftMask,	XK_space}},			togglefloating, {0} },
	{ 1, {{MODKEY,			XK_f}},				togglefullscr,  {0} },
	{ 1, {{MODKEY,			XK_agrave}},			view,           {.ui = ~0 } },
	{ 1, {{MODKEY|ShiftMask,	XK_agrave}},			tag,            {.ui = ~0 } },
	{ 1, {{MODKEY,			XK_comma}},			focusmon,       {.i = -1 } },
	{ 1, {{MODKEY,			XK_semicolon}},			focusmon,       {.i = +1 } },
	{ 1, {{MODKEY|ShiftMask,	XK_comma}},			tagmon,         {.i = -1 } },
	{ 1, {{MODKEY|ShiftMask,	XK_semicolon}},			tagmon,         {.i = +1 } },
	TAGKEYS(                        XK_ampersand,					0)
	TAGKEYS(                        XK_eacute,					1)
	TAGKEYS(                        XK_quotedbl,					2)
	TAGKEYS(                        XK_apostrophe,					3)
	TAGKEYS(                        XK_parenleft,					4)
	TAGKEYS(                        XK_minus,					5)
	TAGKEYS(                        XK_egrave,					6)
	TAGKEYS(                        XK_underscore,					7)
	TAGKEYS(                        XK_ccedilla,					8)
	{ 1, {{MODKEY|ShiftMask,        XK_q}},      			quit,           {0} },
};

/* button definitions */
/* click can be ClkTagBar, ClkLtSymbol, ClkStatusText, ClkWinTitle, ClkClientWin, or ClkRootWin */
static const Button buttons[] = {
	/* click                event mask      button          function        argument */
	{ ClkLtSymbol,          0,              Button1,        setlayout,      {0} },
	{ ClkLtSymbol,          0,              Button3,        setlayout,      {.v = &layouts[2]} },
	{ ClkWinTitle,          0,              Button2,        zoom,           {0} },
	{ ClkStatusText,        0,              Button1,        sigstatusbar,   {.i = 1} },
	{ ClkStatusText,        0,              Button2,        sigstatusbar,   {.i = 2} },
	{ ClkStatusText,        0,              Button3,        sigstatusbar,   {.i = 3} },
	{ ClkStatusText,        0,              Button4,        sigstatusbar,   {.i = 4} },
	{ ClkStatusText,        0,              Button5,        sigstatusbar,   {.i = 5} },
	{ ClkClientWin,         MODKEY,         Button1,        movemouse,      {0} },
	{ ClkClientWin,         MODKEY,         Button2,        togglefloating, {0} },
	{ ClkClientWin,         MODKEY,         Button3,        resizemouse,    {0} },
	{ ClkTagBar,            0,              Button1,        view,           {0} },
	{ ClkTagBar,            0,              Button3,        toggleview,     {0} },
	{ ClkTagBar,            MODKEY,         Button1,        tag,            {0} },
	{ ClkTagBar,            MODKEY,         Button3,        toggletag,      {0} },
};

